﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bulletenemy : MonoBehaviour
{
    [SerializeField] GameObject enemyBullet;
    [SerializeField] Transform spownbullet_Enemy;
    [SerializeField] private double fire_Rate = 1;
    private float fireCounter = 0;
    void FixedUpdate()
    {
        fireCounter += Time.deltaTime;
        if (fireCounter >= fire_Rate)
        {
            Instantiate(enemyBullet, spownbullet_Enemy.position, spownbullet_Enemy.rotation);
            fireCounter = 0;
        }
        
                
    }
}
